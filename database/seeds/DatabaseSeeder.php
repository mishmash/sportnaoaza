<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(FieldsTableSeeder::class);

    	DB::table ('users')->insert([
    		'name'     => 'Damjan',
    		'email'    => 'damjan@sportnaoazatabor.si',
    		'password' =>  bcrypt('kukmak1234#'),
            'role'     =>  'admin'
    	]);

    	DB::table ('reservations')->insert([
    		'name'       => 'DAMJAN',
    		'field_id'   => 1,
    		'user_id'    => 1,
            'res_date'   => '2018-04-13',
    		'start_time' => '07:00:00',
    		'end_time'   => '08:00:00',
    		'status'     => 'approved'
    	]);
    }
}
