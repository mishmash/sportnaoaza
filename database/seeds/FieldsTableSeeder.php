<?php

use Illuminate\Database\Seeder;

class FieldsTableSeeder extends Seeder
{
    public function run()
    {
    	DB::table ('fields')->insert([
    		'slug' => 'igrisce-1',
    		'name' => 'Tenis Igrisce 1',
    		'sport_type' => 'tennis',
    		'open'  => '09:00',
    		'close' => '21:00' 
    	]);

      	DB::table ('fields')->insert([
    		'slug' => 'igrisce-2',
    		'name' => 'Tenis Igrisce 2',
    		'sport_type' => 'tennis',
    		'open'  => '09:00',
    		'close' => '21:00' 
    	]);

    	DB::table ('fields')->insert([
    		'slug' => 'igrisce-3',
    		'name' => 'Tenis Igrisce 3',
    		'sport_type' => 'tennis',
    		'open'  => '09:00',
    		'close' => '21:00' 
    	]);
    }
}
