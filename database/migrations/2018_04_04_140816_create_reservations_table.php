<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('rid');
            $table->string('name')->nullable();
            $table->integer('field_id');
            $table->integer('user_id');
            $table->date('res_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->enum ('status', ['pending', 'approved']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
