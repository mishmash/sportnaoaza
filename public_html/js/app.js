var scroll = new SmoothScroll('a[href*="#"]', {
  offset: 51
});

$( function() {
  $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
} );

$('.confirm').on('click', function() {
  return confirm('Si prepričan?');
});

$('.td-free').on('click', function() {
  const $this = $(this);
  const col = $this.index();
  const row = $this.closest('tr').index();

  const slots = ['7:00', '7:30', '8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00'];
  const courts = ['igrisce-1', 'igrisce-2', 'igrisce-3'];
  const selectedSlot = slots[row];
  const selectedCourt = courts[col - 1];

  $('#tennis-fields').val(selectedCourt);
  $('#time-start').val(selectedSlot);
  $('#time-end').val(slots[row + 2]);
});