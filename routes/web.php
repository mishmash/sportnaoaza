<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/reserve', function() {
	return view('reserve');
});

Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::get ('/logout', 'Auth\LoginController@logout');
Route::get ('/privacy', function() {
	return 'Privacy policy';
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get ('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get ('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::get  ('/reservations/{date?}', 'ReservationsController@reservations');
Route::post ('/reserve', 'ReservationsController@reserve')->name('reservation.create');

Route::get ('/user/confirm/{token}', 'Auth\RegisterController@verifyUser')->name('user.register');

Route::get ('/register/success', function() {
	return view ('auth.register_success');
});

Route::middleware(['auth', 'admin'])->group(function() {
	Route::get ('/admin/dashboard', 'AdminController@dashboard');
	Route::get ('/admin/reservations', 'AdminController@reservations');
	Route::get ('/admin/users', 'AdminController@users');
	Route::get ('/admin/users/member/{id}', 'AdminController@createMember')->name('user.member');
	Route::get ('/admin/reservations/add', 'AdminController@addReservation');
	Route::get ('/admin/reservations/confirm/{rid}', 'AdminController@confirmReservation')->name('reservation.confirm');
	Route::get ('/admin/reservations/delete/{rid}', 'AdminController@deleteReservation')->name('reservation.delete');
	Route::get ('/admin/users/delete/{id}', 'AdminController@deleteUser')->name('user.delete');
	Route::get ('/admin/galleries', 'AdminController@galleries');
	Route::post ('/admin/galleries/add', 'AdminController@addGallery')->name('gallery.add');
	Route::get  ('/admin/galleries/view/{id}', 'AdminController@viewGallery')->name('gallery.view');
	Route::get  ('/admin/galleries/delete/{id}', 'AdminController@deleteGallery')->name('gallery.delete');
	Route::post ('/admin/galleries/upload', 'AdminController@uploadImage')->name('image.add');
	Route::get  ('/admin/images/delete/{id}', 'AdminController@deleteImage')->name('image.delete');
});

/**
 *
 */
Route::get ('/galleries', 'GalleryController@index');

