<ul class="list-inline">
			@foreach ($reservations['days'] as $day)
			<li class="list-inline-item"><a href="/reservations/{{$day['form']}}">
			<?php if ($day['select']): ?>
				<strong style="font-weight: bolder;">
			<?php endif; ?>
				{{$day['name']}}
					<?php if ($day['select']): ?>
				</strong>
			<?php endif; ?>
		</a></li>
			@endforeach
		</ul>

	<table class="table table-reservations" cellpadding="0" cellspacing="0" id="reservation-table">
		<thead class="thead-light">
			<tr>
				<th>Ure</th>
				@foreach ($reservations['fields'] as $field)
				<th>{{$field}}</th>
				@endforeach
			</tr>
		</thead>

		@foreach ($reservations['reservations'] as $reservation)
		<tr>
			<td style="text-align: center;"><small>{{$reservation['slot']}}</small></td>
			@foreach ($reservation['fields'] as $field)
			@if($field['reserved'] != null)
				<td class="td-reserved">{{$field['reserved']['name']}}</td>
			@else
				<td class="td-free"></td>
			@endif
			@endforeach
		</tr>
		@endforeach
	</table>
