	@if (count($reservations) > 0)
	<table class="table table-responsive-md">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>Naziv rezervacije</th>
				<th>Igrišče</th>
				<th>Uporabnik</th>
				<th>Datum</th>
				<th>Termin</th>
				<th>Ustvarjeno</th>
				@if ($action)
				<th>Akcija</th>
				@endif
			</tr>
		</thead>
		<tbody>
			@foreach($reservations as $reservation)
			<tr>
				<th scope="row">{{$reservation->id}}</th>
				<td>{{$reservation->name}}</td>
				<td>{{$reservation->field->name}}</td>
				<td>{{$reservation->user->name}}</td>
				<td>{{\Carbon\Carbon::parse($reservation->res_date)->format('d.m.Y')}}</td>
				<td>
					{{\Carbon\Carbon::parse($reservation->start_time)->format('H:i')}} -
					{{\Carbon\Carbon::parse($reservation->end_time)->format('H:i')}}
				</td>
			<td>{{ $reservation->created_at }}</td>
				@if ($action === true)
				<td>
					<a class="confirm btn btn-success" href="{{route('reservation.confirm', ['rid' => $reservation->rid])}}">
						Potrdi
					</a>
					<a class="confirm btn btn-danger" href="{{route('reservation.delete', ['rid' => $reservation->rid])}}">
						Briši
					</a>
				</td>
				@elseif ($action === 'delete')
				<td>
					<a class="confirm btn btn-danger" href="{{route('reservation.delete', ['rid' => $reservation->rid])}}">
						Briši
					</a>
				</td>
				@endif
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<p>Trenutno ni nobenih čakajočih rezervacij.</p>
	@endif