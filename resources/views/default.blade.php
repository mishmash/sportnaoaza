
<!DOCTYPE html>
<html lang="en-US" >
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Športna Oaza Tabor</title>
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.damjan.cwebspace.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.5"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
}
</style>
<link rel='stylesheet' id='animate-css'  href='css/animate.css?ver=1.0.0' type='text/css' media='all' />
<link rel="stylesheet" href="css/fontawesome-all.min.css">
<link rel='stylesheet' id='owl-carousel-css'  href='css/owl.carousel.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='featuredlite-style-css'  href='css/style.css?ver=4.9.5' type='text/css' media='all' />
<style id='featuredlite-style-inline-css' type='text/css'>
#header .header-img{ background-image:url(img/maxresdefault.jpg); }
</style>
<link rel='stylesheet' id='lfb_f_css-css'  href='css/f-style.css?ver=4.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='onepagepower-parent-style-css'  href='css/flstyle.css' type='text/css' media='all' />
<link rel='stylesheet' id='onepagepower-style-css'  href='css/onepagepower.css?ver=4.9.5' type='text/css' media='all' />
<script type='text/javascript' src='js/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js?ver=1.4.1'></script>
<script type="text/javascript"><!--
function powerpress_pinw(pinw_url){window.open(pinw_url, 'PowerPressPlayer','toolbar=0,status=0,resizable=1,width=460,height=320');	return false;}
//-->
</script>
<script type="text/javascript" src="js/reservation.js"></script>
<style type='text/css'>
.td-reserved {
  background-color: orange;
}
.loader {
    border-top: 2px solid #f16c20;
}
#respond input#submit{
    background:#f16c20;
}
.woocommerce #content input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-page #content input.button:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover, .woocommerce #content input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce-page #content input.button.alt:hover, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page input.button.alt:hover, .woocommerce ul.products li.product a.button:hover, .woocommerce.archive ul.products li.product a.button:hover, .woocommerce-page.archive ul.products li.product a.button:hover,.woocommerce nav.woocommerce-pagination ul li span.current,
.woocommerce-page nav.woocommerce-pagination ul li a:hover,.woocommerce nav.woocommerce-pagination ul li a:focus,.woocommerce-page nav.woocommerce-pagination ul li a:focus {
    background:#f16c20;
}
.woocommerce ul.products li.product a.button, .woocommerce.archive ul.products li.product a.button, .woocommerce-page.archive ul.products li.product a.button {
   background:#f16c20;
}
.home .switch-lead .leadform-show-form.leadform-lite input[type="submit"] {
     background-color:#f16c20!important; 
}
.home .switch-lead .lead-genration-wrapper .popup .close {
   color: #f16c20; 
}
.last-btn .navigation ul#menu > li:last-child > a{
    border: 2px solid #f16c20; 
    color: #f16c20; 
}
.last-btn .menu-item-has-children > a:after{
color: #f16c20; 
}
.last-btn.smaller .navigation ul#menu > li:last-child > a{
 border: 2px solid #f16c20; 
 background:#f16c20   
}
blockquote:before,
ul.multiple-testimonial-section li.multi-testimonial blockquote:before, a:hover, a:focus {
 color: #f16c20;
}
span.multi-featured-icon i, #move-to-top .fa-angle-up:before {
    color: #f16c20;
}
span.multi-testimonial-image img {
    box-shadow: 0px 0px 0px 1px #f16c20;
}
ul.multiple-featured-section li:hover span.multi-featured-icon i {
    background-color: #f16c20;
    color: #fff;
    border-color: #f16c20;
}
ul.multiple-testimonial-section li.multi-testimonial a.author{
 color: #f16c20!important;
}
.wpcf7 input[type=submit] {
background:#f16c20;
}
input.search-submit {
background:#f16c20;
}
.newsletter-wrapper form.Newsletter-form input[type="submit"] {
    background: #f16c20;
    color: #fff;
}
.blog-container .blog-loop .read-more a, .single-container .single-loop .read-more a {
    border: 1px solid #f16c20;
    color: #f16c20;
}
.blog-container .blog-loop .read-more a:hover, .single-container .single-loop .read-more a:hover, .tagcloud a {
    background-color: #f16c20;
}
.blog-container .breadcrumb h4, .single-container .breadcrumb h4{
border-bottom: 2px solid #f16c20;
}
.contact-wrap .form-group:before {
 background: #f16c20;
}
.contact-wrap .form-group.form-lined:before {
background: #f16c20;
}
.contact-wrap .leadform-show-form.leadform-lite input[type="submit"]{
 background: #f16c20;
}
.widgettitle > span:before, .widgettitle > span:after{
    border-color: #f16c20;
}
.widgettitle > span:before, .widgettitle > span:after, .page .breadcrumb h1:after, #move-to-top, .widgettitle, .tagcloud a, h1.page-title:after  {
    border-color: #f16c20!important;
}
.woocommerce span.onsale, .woocommerce-page span.onsale {   
 background-color:#f16c20}
.woocommerce-page #respond input#submit {
    background:#f16c20}
.woocommerce ul.products li.product a.button{   
 background-color:#f16c20}
.woocommerce ul.products li.product a.button:hover{   
 background-color:#f16c20}
 .woocommerce-page a.button{
background-color:#f16c20 }
.woocommerce div.product form.cart .button {
background-color:#f16c20     
}
.woocommerce .cart .button, .woocommerce .cart input.button,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt{
background-color:#f16c20      
}
.footer-wrapper{
 background:   
}
.footer-copyright{
 background:      
}
.header-wrapper{
 background:      
}
header.smaller .header-wrapper, 
.home .hdr-transparent.smaller .header-wrapper{
background:}
.logo h1 a,.logo p,
.split-menu .logo-cent h1 a,
.split-menu .logo-cent p{
color:    
}
.navigation .menu > li > a,
.menu-item-has-children > a:after{
color:#fff   
}
.navigation ul li a:hover,
.navigation .menu .current_page_item a, 
.navigation .menu li a.active{
color:#f16c20    
}
@media screen and (max-width: 1024px){
.navigation ul li a:hover,
.navigation .menu .current_page_item a,
.navigation .menu li a.active{
 background:#f16c20; 
 color:#fff!important;
}
.header a#pull{
 color:   
}
}

.main-header-section{
 background-color:rgba(0, 0, 0, 0.3)   
}
.main-header-section .main-text h1{
 color:   
}
.main-header-section .main-text h2{
 color:      
}
.main-header-section .main-head-partition{
 border-color:      
}
.featured-section.first a:hover{
background-color:   	
}
.featured-section.first span.featured-icon i{
color:;	
border-color:}
.featured-section.first h3{
color:	
}
.featured-section.first p{
color:		
}

.featured-section.second a:hover{
background-color:   	
}
.featured-section.second span.featured-icon i{
color:;	
border-color:}
.featured-section.second h3{
color:	
}
.featured-section.second p{
color:		
}
.featured-section.third a:hover{
background-color:   	
}
.featured-section.third span.featured-icon i{
color:;	
border-color:}
.featured-section.third h3{
color:	
}
.featured-section.third p{
color:		
}

.ribbon-section{
background: ;
}
.ribbon-section .ribbon-button button:hover{
    background: ;
    color: ;
}
.ribbon-section h2.heading-area{
   color: 
}
.ribbon-section .ribbon-button button{
    background: ;
    color: ;
}
.bottom-ribbon-section{
 background:;
}
.bottom-ribbon-section .ribbon-button button:hover {
    background:;
    color:;
}
.bottom-ribbon-section .ribbon-button button{
    background:;
    color:;
}
.bottom-ribbon-section h2.heading-area{
    color:;
}

.multi-feature-area {
    background-color: ;
}
.multi-feature-area h2.head-text{
    color:}
.multi-feature-area h3.subhead-text{
    color:}

ul.multiple-featured-section li.multi-featured{
background:}

.multi-feature-area h3{
    color:}
ul.multiple-featured-section li.multi-featured p{
    color:}
.aboutus-section {
    background:  ;
}
.aboutus-text h2, .aboutus-text p{
    color: ;
}
.client-team-section  {
  background:;
}
.client-team-section h2 {
        color: ;
}
.client-team-section h3 {
        color: ;
}
#woocommerce.woocommerce-section{
background:;  
}
.woocommerce-section h2{
color:    
}
.woocommerce-section h3{
color:        
}
.client-testimonial-section {
    background: ;
}
.client-testimonial-section h2 {
	    color: ;
}
#testimonials h3.subhead-text{
        color: ;
}
#news.multi-slider-area{
  background:  rgba(0,0,0,0.3)}
#news.multi-slider-area h2.head-text{
   color:}
#news.multi-slider-area h3.subhead-text{
    color:}
.contact-section{
 background:rgba(0, 0, 0, 0.3)   
}
.contact-section h2{
 color: ;   
}
.contact-section h3.subhead-text{
    color: ;
}
.cnt-detail .cnt-icon i, .cnt-detail .cnt-info a, .cnt-info p{
    color: #ffffff;
}
</style>		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		  </head>
  <body id="page-top" class="home page-template page-template-home-template page-template-home-template-php page page-id-7 wp-custom-logo" >
    <div class="overlayloader">
      <div class="loader">&nbsp;</div>
    </div>
    <!-- Start Of Header Wrapper -->
        <!-- script to split menu -->
   
   <!-- script to split menu --> 
    <header id="header" class=" menu-center   ">
      <div class="header-img">
      <div class="overlay-demo"></div>
        </div>
        <div class="header-wrapper" >
          <!-- Start Of Top Container -->
          <div class="container">
            <div class="header">
              <!-- Start Of Logo -->
              <div class="logo">
                <div class="logo-img">
                 
                                     <a href="http://www.sportnaoazatabor.si/" class="custom-logo-link" rel="home" itemprop="url"><img width="225" height="192" src="http://www.damjan.cwebspace.com/wp-content/uploads/2018/01/cropped-rsz_sot_logo_white.png" class="custom-logo" alt="Športna Oaza Tabor" itemprop="logo" /></a>                </div>
              </div>
              <!-- End Of Logo -->
              <div id="main-menu-wrapper">
                <a href="#" id="pull" class="toggle-mobile-menu"></a>
                <nav class="navigation clearfix mobile-menu-wrapper">
                  <ul id="menu" class="menu"><li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-7 current_page_item menu-item-29"><a class="page-scroll" href="http://www.sportnaoazatabor.si">Domov</a></li>
<li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a class="page-scroll" href="#onas">O Nas</a></li>

<!--<li id="menu-item-31" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-31"><a class="page-scroll"href="#">Storitve</a>
<ul class="sub-menu">
	<li id="menu-item-71" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-71"><a class="page-scroll"href="#">Gostinska ponudba</a></li>
	<li id="menu-item-72" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-72"><a class="page-scroll"href="#">Tenis</a></li>
	<li id="menu-item-73" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-73"><a class="page-scroll"href="#">Rekreacijska liga tenisa</a></li>
	<li id="menu-item-74" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74"><a class="page-scroll"href="#">Rekreacijska liga malega nogometa</a></li>
	<li id="menu-item-75" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-75"><a class="page-scroll"href="#">Praznovanja</a></li>
	<li id="menu-item-76" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-76"><a class="page-scroll"href="#">Rezervacija igrisc</a></li>
</ul>
</li>-->
<li id="menu-item-32" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-32"><a class="page-scroll"href="#reserve">Rezervacija</a></li>
<!--
<li id="menu-item-34" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34"><a class="page-scroll"href="#">Prijava</a></li>
<li id="menu-item-35" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35"><a class="page-scroll"href="#">Registracija</a></li> -->
<li id="menu-item-36" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-36"><a class="page-scroll"href="#kontakt">Kontakt</a></li>
</ul>                </nav>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
    </header>
    @yield('content')
<!-- team section end --><div class="clearfix"></div>
<input type="hidden" id="back-to-top" value=""/> 
             </div>
<div class="footer-copyright">
<div class="container">
	<ul>
		<li class="copyright">
			Športna Oaza Tabor | 2018.		</li>
		<li class="social-icon">

<!--
<ul>
  <li><a target='_blank' href="#" ><i class='fa fa-twitter'></i></a></li>
  <li><a target='_blank' href="#" ><i class='fa fa-google-plus'></i></a></li>
  <li><a target='_blank' href="#" ><i class='fa fa-linkedin'></i></a></li>
  <li><a target='_blank' href="#" ><i class='fa fa-pinterest'></i></a></li>    
</ul>
-->
					</li>
	</ul>
</div>
</div>
<script type='text/javascript' src='js/wow.js?ver=4.9.5'></script>
<script type='text/javascript' src='js/easing.js?ver=4.9.5'></script>
<script type='text/javascript' src='js/classie.js?ver=4.9.5'></script>
<script type='text/javascript' src='js/owl.carousel.js?ver=4.9.5'></script>
<script type='text/javascript' src='js/custom.js?ver=4.9.5'></script>
<script type='text/javascript' src='js/comment-reply.min.js?ver=4.9.5'></script>
<script type='text/javascript' src='js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script type='text/javascript' src='js/f-script.js?ver=1.0.0'></script>
<script type='js/wp-embed.min.js?ver=4.9.5'></script>
</body>
</html>