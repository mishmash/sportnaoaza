@extends ('layouts.app')
@section('content')
<div class="container pt-3">
	@foreach ($galleries as $gallery)

	@if ($gallery->images()->count() > 0)
	<h2>{{ $gallery->name }}</h2>
	<div class="row">
			@foreach ($gallery->images as $image)
			<div class="col-md-3">
				<a rel="{{ $gallery->slug }}" class="" href="{{ url($image->path) }}" target="_blank">
					<img src="{{ url($image->path) }}" class="img-thumbnail img-fluid">
				</a>
			</div>
			@endforeach
	</div>
	@endif
	{{-- <div class="row">
		<div style="width: 100%;" class="row">
			<h2>{{ $gallery->name }}</h2> <br />
		</div>

		<div class="row">
			<ul class="gallery-images">
			@foreach ($gallery->images as $image)
			<li class="gallery-images">
				<a rel="{{$gallery->slug}}" class="swipebox" href="{{ url($image->path) }}" target="_blank">
					<img src="{{ url($image->path) }}" alt="image">
				</a>
			</li>
			@endforeach
			</ul>
		</div>
	</div>
	@endif--}}

	@endforeach
</div>
@endsection