@extends ('layouts.app')
@section('content')

<section id="reserve" class="pt-3 pb-0">
  <div class="container">
    @if (Auth::check())
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif

    @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
    @endif
    <h2>Rezervacija igrišča</h2>
      <div class="row">
        <div class="col-6">
          {{ Form::open(['route' => 'reservation.create']) }}
            <div class="form-group">
              <label for="tennis-fields">Tenis igrišče</label>
              {{Form::select('field', $reservations['fields'], null, ['class' => 'form-control', 'id' => 'tennis-fields'])}}
            </div>
            <div class="form-group">
              <label for="date">Datum</label>
              {{Form::text('date', $today, ['class' => 'form-control', 'id' => 'datepicker'])}}
            </div>
            <div class="form-group">
              <label for="time-start">Začetek termina</label>
              {{Form::select('time_start', ['7:00' => '7:00', '7:30' => '7:30', '8:00' => '8:00', '8:30' => '8:30', '9:00' => '9:00', '9:30' => '9:30', '10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30', '12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30', '14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30', '16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30', '18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30', '20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00'], null, ['class' => 'form-control', 'id' => 'time-start'])}}
            </div>
            <div class="form-group">
              <label for="time-end">Konec termina</label>
              {{Form::select('time_end', ['7:00' => '7:00', '7:30' => '7:30', '8:00' => '8:00', '8:30' => '8:30', '9:00' => '9:00', '9:30' => '9:30', '10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30', '12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30', '14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30', '16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30', '18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30', '20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00'], null, ['class' => 'form-control', 'id' => 'time-end'])}}
            </div>
            @if ($phone == false)
            <div class="form-group">
              <label for="Phone">Telefonska številka</label>
              {{Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone', 'placeholder' => 'Telefonska številka'])}}
            </div>
            @else
              {{Form::hidden('phone', $phone)}}
            @endif
            {{Form::submit('Rezerviraj', ['class' => 'btn btn-primary'])}}
          {{Form::close()}}
    @else
          <div class="alert alert-danger" role="alert">
          Za ustvarjanje rezervacije se <a href="/login">prijavite</a> ali pokličite 041 545 504
          </div>
    @endif
        </div>
      </div>
  </div>
</section>


<!-- #
     # Start of blade comment 
     # -->

  {{--

  <h1>Rezervacija igrišča</h1>
  {{ Form::open(['route' => 'reservation.create']) }}
  <table class="table">
    <!--
    <thead>
      <th>Igrišče</th>
      <th>Datum</th>
      <th>Začetek</th>
      <th>Konec</th>
    </thead>
    -->
    <tbody>
      <tr>
        <td>{{Form::select('field', $reservations['fields'], ['class' => 'reset-this'])}}</td>
      </tr>
      <tr>
        <td>{{Form::text("date", $today, array("id" => "datepicker"))}}</td>
      </tr>
      <tr>
        <td>{{Form::text("time_start", "", array("id" => "time_start", "class" => "timepicker", "placeholder" => "Začetek termina"))}}</td>
      </tr>
      <tr>
        <td>{{Form::text("time_end", "", array("id" => "time_end", "class" => "timepicker", "placeholder" => "Konec termina"))}}</td>
      </tr>
      @if ($phone == false)
      <tr>
        <td>{{Form::text("phone", "", ["placeholder" => "Telefonska številka"])}}</td>
      </tr>
      @else
      <tr>
        <td>{{Form::hidden("phone", $phone)}}</td>
      </tr>
      @endif
      <tr>
        <td>
          {{Form::submit('Rezerviraj', ['class' => 'btn btn-info'])}}
        </td>
      </tr>
      </tbody>
    </table>
    {{Form::close()}}
  @else
    <p>Za ustvarjanje rezervacije se <a style="color: orange;" href="/login">prijavite</a> ali pokličite 041 545 504</p>
  @endif
  
  --}}
<!-- 
     #
     # End of blade comment 
     # -->

<section id="reservations" class="pt-0">
  <div class="container">
    <h2>Trenutno stanje rezervacij</h2>
    @include ("shared.courts")
  </div>
</section>
@endsection