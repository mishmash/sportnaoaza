
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Športna Oaza Tabor</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/bootstrap-social.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.4/css/swipebox.min.css" />
    <link href="/css/sot.css" rel="stylesheet" />




    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 54px;
      }
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }

    </style>

  </head>

  <body id="sot">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Športna Oaza Tabor</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="/">Domov
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/#onas">O Nas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/reservations">Rezervacije</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/galleries">Galerije</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/#kontakt">Kontakt</a>
            </li>

            @if (Auth::check())

            @if (Auth::user()->role == "admin")
            <li class="nav-item">
              <a class="nav-link" href="/admin/dashboard">
                <strong>Admin</strong>
              </a>
            </li>
            @endif

            <li class="nav-item">
              <a class="nav-link" href="/logout">Odjava</a>
            </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          @yield('content')
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="/js/reservation.js"></script>
    <script type='text/javascript' src='/js/datepicker.min.js?ver=1.11.4'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.4/js/jquery.swipebox.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    jQuery('.swipebox').swipebox();
    </script>

  </body>

</html>
