
<!--<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ŠOT Administracija</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/simple-sidebar.css" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style type="text/css">
    .gallery-images img {
        width: 240px;
        height: 160px;
        border: 0px solid black;
        margin-bottom: 10px;
    }

    .gallery-images ul {
        margin: 0;
        padding: 0;
    }

    .gallery-images li {
        margin: 0;
        padding: 0;
        list-style: none;
        float: left;
        padding-right: 10px;
    }
    </style>

    <script src="/js/jquery.js"></script>
    <script type='text/javascript' src='/js/datepicker.min.js?ver=1.11.4'></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script type="text/javascript">
        var baseUrl = "{{url('/')}}/";
        jQuery(document).ready(function() {
            jQuery('.confirm').on('click', function() {
                return confirm ('Si prepričan?');
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js" type="text/javascript"></script>
    <script src="/js/reservation.js" type="text/javascript"></script>

</head>

<body>

    <div id="wrapper" class="toggled">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="/admin/dashboard">
                        Športna Oaza Tabor
                    </a>
                </li>
                <li>
                    <a href="/admin/reservations">
                        Rezervacije
                    </a>
                </li>
                <li>
                    <a href="/admin/reservations/add">
                        Dodaj rezervacijo
                    </a>
                </li>
                <li>
                    <a href="/admin/users">
                        Uporabniki
                    </a>
                </li>

                <li>
                    <a href="/admin/galleries">
                        Galerije
                    </a>
                </li>

                <li>
                    <a href="/logout">
                        Odjava
                    </a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <!--
            <div class="container-fluid">
                <h1>Simple Sidebar</h1>
                <p>This template has a responsive menu toggling system. The menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will appear/disappear. On small screens, the page content will be pushed off canvas.</p>
                <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>.</p>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Toggle Menu</a>
            </div>
            -->
            @include ('shared.status ')
            @yield('content')
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="/js/bootstrap.bundle.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    jQuery("#menu-toggle").click(function(e) {
        e.preventDefault();
        jQuery("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>
