<!doctype html>
<html lang="sl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/bootstrap-social.css">
  <link rel="stylesheet" href="/css/jquery-ui.css">
  <link rel="stylesheet" href="/css/custom.css">
  <title>Športna Oaza Tabor</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/">Športna Oaza Tabor</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link d-none d-lg-block" href="/#home">Domov</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#about" data-scroll>O nas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#services" data-scroll>Storitve</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#pricing" data-scroll>Cenik</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/galleries" data-scroll>Galerija</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#contact" data-scroll>Kontakt</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          @if(!Auth::check())
          <li class="nav-item">
            <a class="nav-link" href="/login">Prijava</a>
          </li>
          <li class="nav-item">
  	        <a class="nav-link" href="/register">Registracija</a>
          </li>
          @else
          @if (Auth::user()->role == "admin")
            <li class="nav-item">
              <a class="nav-link" href="/admin/reservations">Admin</a>
            </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="/logout">Odjava</a>
          </li>
          @endif
          <li class="nav-item">
            <a href="/reservations" class="btn btn-outline-primary">Rezervacija igrišč</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  @yield('content')

  <footer class="text-center mt-auto py-3">
    <div class="container">
      <p class="white-text">@ 2019 Copyright Športna Oaza Tabor</p>
    </div>
  </footer>

  <script src="/js/jquery-3.4.0.min.js"></script>
  <script src="/js/jquery-ui.min.js"></script>
  <script src="/js/popper.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/smooth-scroll.polyfills.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script src="/js/app.js"></script>
</body>

</html>