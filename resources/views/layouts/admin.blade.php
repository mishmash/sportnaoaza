<!doctype html>
<html lang="sl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
    integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/bootstrap-social.css">
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css" />
  <link rel="stylesheet" href="/css/custom.css">
  <title>Športna Oaza Tabor</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/">Športna Oaza Tabor</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="/admin/reservations">Rezervacije</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/reservations/add" data-scroll>Dodaj rezervacijo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/users" data-scroll>Uporabniki</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/galleries" data-scroll>Galerije</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/logout" data-scroll>Odjava</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container pt-3">
    @include ('shared.status ')
    @yield('content')
  </div>

  <footer class="text-center mt-auto py-3">
    <div class="container">
      <p class="white-text">@ 2019 Copyright Športna Oaza Tabor</p>
    </div>
  </footer>

  <script src="https://code.jquery.com/jquery-3.4.0.min.js"
  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
  crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/smooth-scroll.polyfills.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js" type="text/javascript"></script>
  <script src="/js/app.js"></script>
</body>

</html>