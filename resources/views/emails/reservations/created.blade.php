Podana je bila zahteva za novo rezervacijo.<br/><br/>

Oseba {{ $user }} (<strong>{{ $role }}</strong>)
 (tel: {{ $phone }}) je podala zahtevo za rezervacijo.<br/><br/>

Rezervacija za <strong>{{ $field }}</strong> dne <strong>{{$date}}</strong> in<br/>
sicer od <strong>{{ $from }}</strong> do <strong>{{ $to }}</strong>.<br/><br/>

Za takojšno potrditev klikni spodnjo povezavo:<br/>
<strong>
	<a href="{{ route('reservation.confirm', ['rid' => $rid]) }}">
		Potrdi rezervacijo
	</a>
</strong>