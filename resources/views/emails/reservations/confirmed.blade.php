Pozdravljeni,<br/><br/>

Vaša registracija za igrišče <strong>{{ $field }}</strong> za dne <strong>{{ $date }}</strong><br/>
od <strong>{{$from}}</strong> do <strong>{{ $to }}</strong> je sedaj potrjena. Veselimo se vašega<br/>
obiska in vam želimo veliko užitka ob igranju!<br/><br/>

Lep pozdrav,<br/>
Športna Oaza Tabor