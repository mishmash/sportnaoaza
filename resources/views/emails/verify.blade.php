Pozdravljeni,<br/><br/>

prosim kliknite spodnjo povezavo in s tem potrdite registracijo<br/>
na spletni strani Športna Oaza Tabor.<br/><br/>

{{ route('user.register', ['token' => $token]) }}<br/><br/>

Hvala za registracijo,<br/>
želimo vam prijetno igranje.<br/><br/>

Športna Oaza Tabor