@extends ('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registracija uspešna') }}</div>

                <div class="card-body">
                    <p>Preden se lahko prijavite morate potrditi račun tako da stisnete
                    na povezavo ki ste jo prejeli na vaš email.</p>
                    <p>Hvala za registracijo in veliko užitka ob igranju</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection