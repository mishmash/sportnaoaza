@extends ('layouts.app')
@section('content')
<div id="home" class="jumbotron jumbotron-fluid d-none d-lg-block">
    <div class="container text-center">
      <h2 class="mb-5">Športna Oaza Tabor</h2>
      <div class="row">
        <div class="col-md-4">
          <div class="jumbotron-box">
            <i class="fas fa-info-circle fa-3x mb-3 mt-2"></i>
            <h2>O nas</h2>
            <p>Športni center, kateri trenutno razpolaga z tremi peščenimi igrišči za tenis in eno dotrajano nogometno asfaltno igrišče, bi radi v prihodnosti urediti,da bi mestna četrt Tabor spet zaživela v vsem svoj sijaju, kot je včasih tudi bilo.</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="jumbotron-box">
            <i class="fas fa-check fa-3x mb-3 mt-2"></i>
            <h2>Storitve</h2>
            <p>Gostinska ponudba | tenis | rekreacijska liga tenisa | rekreacijska liga malega nogometa | praznovanja | rezervancija igrisc.</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="jumbotron-box">
            <i class="fas fa-lightbulb fa-3x mb-3 mt-2"></i>
            <h2>Ideja</h2>
            <p>Maribor zaostaja za smernicami, ki jih določa nacionalni program športa glede površine športnih objektov na prebivalca. Do leta 2020 bi morali v Mariboru zgraditi dodatnih 8.500 m2 pokritih športnih površin.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section id="about" class="text-center">
    <div class="container">
      <div class="section-heading">
        <h2>O nas</h2>
        <p class="subheading grey-text">in naša zgodba</p>
      </div>
      <div class="section-content">
        <div class="row">
          <div class="col-md-6">
            <p class="lead">Športna Oaza Tabor (prej DŠR Železničar Maribor), ki je bilo nekoč polno otroškega smeha in
              polno rekreativnih igralcev, je bil prostor, kjer so se športniki družili in sodelovali v raznih športnih
              panogah,a danes je zapuščeno in propada. Igrišče sva kupila z namenom, da mu vrneva stari blišč in
              krajevni skupnosti, ter širši okolici mesta Maribor omogočiva dostop do novih športnih površin v več
              športih. Športni center, kateri trenutno razpolaga z tremi peščenimi igrišči za tenis in eno dotrajano
              nogometno asfaltno igrišče, bi radi v prihodnosti urediti,da bi mestna četrt Tabor spet zaživela v vsem
              svoj sijaju, kot je včasih tudi bilo. Prav tako,bi radi vrnili rekreativno ligo nogometa in tenisa, katera
              je včasih na teh igriščih zelo živela.</p>
          </div>
          <div class="col-md-6">
            <img src="img/oaza.jpg" class="img-fluid" />
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="ribbon text-center">
    <div class="container">
      <p class="white-text">Mi gradimo, vi se reklamirate. Kontaktirajte nas na številko 041 545 504.</p>
    </div>
  </section>

  <section id="services" class="text-center">
    <div class="container">
      <div class="section-heading">
        <h2>Storitve</h2>
        <p class="subheading grey-text">ki jih ponujamo</p>
      </div>
      <div class="section-content">
        <div class="row">
          <div class="col-md-4 mb-3">
            <i class="fas fa-coffee fa-3x orange-text"></i>
            <h3>Gostinska ponudba</h3>
            <!--<p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam
              minima assumenda deleniti hic.</p>-->
          </div>
          <div class="col-md-4 mb-3">
            <i class="fas fa-baseball-ball fa-3x orange-text"></i>
            <h3>Tenis</h3>
            <!--<p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam
              minima assumenda deleniti hic.</p>-->
          </div>
          <div class="col-md-4 mb-3">
            <i class="fas fa-trophy fa-3x orange-text"></i>
            <h3>Rekreacijska liga tenisa</h3>
            <!--<p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam
              minima assumenda deleniti hic.</p>-->
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 mb-3">
            <i class="fas fa-award fa-3x orange-text"></i>
            <h3>Rekreacijska liga malega nogometa</h3>
            <!--<p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam
              minima assumenda deleniti hic.</p>-->
          </div>
          <div class="col-md-4 mb-3">
            <i class="fas fa-birthday-cake fa-3x orange-text"></i>
            <h3>Praznovanja</h3>
            <!--<p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam
              minima assumenda deleniti hic.</p>-->
          </div>
          <div class="col-md-4 mb-3">
            <i class="far fa-calendar-alt fa-3x orange-text"></i>
            <h3>Rezervacija igrisc</h3>
            <!--<p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam
              minima assumenda deleniti hic.</p>-->
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="pricing" class="text-center">
    <div class="container">
      <div class="section-heading">
        <h2>Cenik</h2>
        <p class="subheading grey-text">za vsak žep</p>
      </div>
      <div class="section-content">
        <div class="card-deck">
          <div class="card box-shadow">
            <div class="card-header">
              <h4>Urna postavka</h4>
            </div>
            <div class="card-body">
              <p class="pricing-price">7€ <small class="text-muted">/ uro</small></p>
              <ul class="list-unstyled mt-3 mb-3">
                <li>Cena velja za celotno igrišče</li>
                <li>Igranje tenisa pod reflektorji: doplačilo 2€</li>
              </ul>
            </div>
          </div>
          <div class="card box-shadow">
            <div class="card-header">
              <h4>Dopoldanski termin</h4>
            </div>
            <div class="card-body">
              <p class="pricing-price">120€</p>
              <ul class="list-unstyled mt-3 mb-3">
                <li>Od 7:00 do 15:00</li>
                <li>Rezervacija 2 uri tedensko</li>
                <li>Cena velja za celotno igrišče</li>
                <li>Igranje tenisa pod reflektorji: doplačilo 2€</li>
              </ul>
            </div>
          </div>
          <div class="card box-shadow">
            <div class="card-header">
              <h4>Popoldanski termin</h4>
            </div>
            <div class="card-body">
              <p class="pricing-price">150€</p>
              <ul class="list-unstyled mt-3 mb-3">
                <li>Od 15:00 do 21:00</li>
                <li>Rezervacija 2 uri tedensko</li>

                <li>Cena velja za celotno igrišče</li>
                <li>Igranje tenisa pod reflektorji: doplačilo 2€</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--<section id="gallery" class="text-center">
    <div class="container">
      <div class="section-heading">
        <h2>Galerija</h2>
        <p class="subheading grey-text">za lušte</p>
      </div>
      <div class="row text-center text-lg-left">
        <div class="col-md-4">
          <img class="img-fluid img-thumbnail"
            src="https://scontent.flju2-2.fna.fbcdn.net/v/t31.0-8/30051581_159605678070062_8562799336814862923_o.jpg?_nc_cat=105&_nc_ht=scontent.flju2-2.fna&oh=78916d10d0daafad6e97065954396a29&oe=5D2CDBBE" />
        </div>
        <div class="col-md-4">
          <img class="img-fluid img-thumbnail"
            src="https://scontent.flju2-1.fna.fbcdn.net/v/t31.0-8/30425200_159605481403415_8531663777035366632_o.jpg?_nc_cat=109&_nc_ht=scontent.flju2-1.fna&oh=344ed1ccd2a2d7ea0c3146d78c0f65c4&oe=5D2C8448" />
        </div>
        <div class="col-md-4">
          <img class="img-fluid img-thumbnail"
            src="https://scontent.flju2-1.fna.fbcdn.net/v/t1.0-9/30727509_161151724582124_5393210070175637877_n.jpg?_nc_cat=109&_nc_ht=scontent.flju2-1.fna&oh=8572bb9945b65702cb54e75cb34517f1&oe=5D382084" />
        </div>
      </div>
    </div>
  </section>-->

  <section id="contact" class="text-center">
    <div class="container">
      <div class="section-heading">
        <h2>Kontakt</h2>
        <p class="subheading grey-text">kontakt</p>
      </div>
      <div class="section-content">
        <div class="row">
          <div class="col-md-4">
              <div class="media mb-3">
                  <i class="fas fa-home fa-2x orange-text mr-4"></i>
                  <div class="media-body text-left">
                      <p>Cesta zmage 90<br>2000 Maribor<br>Slovenija</p>
                  </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="media mb-3">
                  <i class="fas fa-phone fa-2x orange-text mr-4"></i>
                  <div class="media-body text-left">
                      <p>+386 41 545 504<br>
                        +386 41 822 788</p>
                  </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="media mb-3">
                  <i class="fas fa-envelope fa-2x orange-text mr-4"></i>
                  <div class="media-body text-left">
                      <p>info@sportnaoazatabor.si</p>
                  </div>
              </div>
          </div>
        </div>
        <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2744.057949176534!2d15.634998515756077!3d46.546540468591125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476f7798c4757193%3A0x71e30f7f012730cd!2sCesta+zmage%2C+2000+Maribor%2C+Slovenija!5e0!3m2!1shr!2shr!4v1517390435256" width="100%" height="300" frameborder="0" style="border: 0px; pointer-events: none;" allowfullscreen=""></iframe></div>
      </div>
    </div>
  </section>
@endsection