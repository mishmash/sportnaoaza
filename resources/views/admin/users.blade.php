@extends ('layouts.admin')
@section ('content')
<h2>Uporabniki</h2>
<p>To so samo registrirani uporabniki in ne člani.</p>

<table class="table table-responsive-md">
	<thead class="thead-dark">
		<tr>
			<th>Uporabnik</th>
			<th>Email</th>
			<!-- <th>Status</th> -->
			<th>Telefon</th>
			<th>Akcija</th>
		</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr>
			<td>{{$user->name}}</td>
			<td>{{$user->email}}</td>
			<!-- <td>{{$user->verified ? 'Potrjen' : 'Nepotrjen'}}</td> -->
			<td>{{$user->phone}}</td>
			<td>
				<a class="confirm btn btn-primary" href="{{ route('user.member', ['id' => $user->id]) }}">
					Dodaj članstvo
				</a>
				
				<a class="confirm btn btn-danger" href="{{ route('user.delete', ['id' => $user->id]) }}">
					Briši
				</a>
			</td>
		</tr>
	</tbody>
	@endforeach
</table>
<h2>Člani</h2>
<p>Ti uporabniki so člani Športne Oaze Tabor.</p>

<table class="table table-responsive-md">
	<thead class="thead-dark">
		<tr>
			<th>Uporabnik</th>
			<th>Email</th>
			<!-- <th>Status</th> -->
			<th>Telefon</th>
			<th>Akcija</th>
		</tr>
	</thead>
	<tbody>
	@foreach($members as $user)
		<tr>
			<td>{{$user->name}}</td>
			<td>{{$user->email}}</td>
			<!-- <td>{{$user->verified ? 'Potrjen' : 'Nepotrjen'}}</td> -->
			<td>{{$user->phone}}</td>
			<td>
				<a class="confirm btn btn-primary" href="{{ route ('user.member', ['id' => $user->id])}}">
					Ni več član
				</a>
				<a class="confirm btn btn-danger" href="{{ route('user.delete', ['id' => $user->id]) }}">
					Briši
				</a>
			</td>
		</tr>
	</tbody>
	@endforeach
</table>
@stop