@extends ('layouts.admin')
@section ('content')

<h2>Galerije</h2>
<div class="row pb-3">
	<div class="col-md-6">
			<form action="/admin/galleries/add" method="post">
				@csrf
				<div class="form-group">
					<label for="gallery-name">Tenis igrišče</label>
					<input id="gallery-name" class="form-control" placeholder="Ime galerije" type="text" name="gallery_name" required autofocus>
				</div>
				<input type="submit" class="btn btn-primary" value="Dodaj galerijo" />
			</form>
	</div>
</div>

<table class="table pt-3">
	<thead>
		<th>Ime galerije</th>
		<th>&nbsp;</th>
	</thead>
	<tbody>
		@foreach ($galleries as $gallery)
		<tr>
			<td>{{$gallery->name}}</td>
			<td>
				<a href="{{ route ('gallery.view', ['id' => $gallery->slug]) }}">Ogled</a> |
				<a class="confirm" href="{{ route ('gallery.delete', ['id' => $gallery->slug]) }}">
					<i class="fa fa-trash" aria-hidden="true"></i>
				</a>

				<span class="pull-right">
					{{ $gallery->images()->count() }}
				</span>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection