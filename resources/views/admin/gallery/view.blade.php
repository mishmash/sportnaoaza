@extends ('layouts.admin')
@section ('content')
<div class="col-md-12">
		<h2>
			<a href="/admin/galleries">
				<<
			</a>
			Ogled galerije: {{ $gallery->name }}
		</h2>
		<h3>Dodaj slike</h3>
	
		<form action="{{ route('image.add') }}"
			class="dropzone" 
			id="addImages">
			@csrf
	
			<input type="hidden" name="gallery_slug" value="{{ $gallery->slug }}" />
			<div class="dz-message" data-dz-message>
				<span>Klikni za dodajanje slik</span>
			</div>
		</form>
	
		<h3>Obstoječe slike</h3>
		<div class="gallery-images">
			<div class="row">
				@foreach ($gallery->images as $image)
					<div class="col-md-4 mb-3">
						<img src="{{ url($image->path) }}" class="img-thumbnail img-fluid" />
						<a href="{{ route('image.delete', ['id' => $image->id]) }}" class="btn btn-danger btn-block confirm">Odstrani</a>
					</div>
				@endforeach
			</div>
			{{--<ul>
				@foreach ($gallery->images as $image)
				<li>
					<div>
						{{ $image->original_name }}
						<a class="confirm pull-right" href="{{ route('image.delete', ['id' => $image->id]) }}">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</div>
	
					<div>
					<a href="{{ url($image->path) }}" target="_blank">
						<img src="{{ url($image->path) }}" alt="image">
					</a>
					</div>
				</li>
				@endforeach
			</ul>--}}
		</div>
	</div>
@endsection