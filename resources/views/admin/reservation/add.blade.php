@extends ('layouts.admin')
@section ('content')

@if ($errors->any())
<div class="alert alert-danger">
		<ul>
				@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
				@endforeach
		</ul>
</div>
@endif

<h2>Dodaj rezervacijo</h2>
<p>Rezervacije dodane preko tega vmesnika so <strong>samodejno potrjene</strong>.
<div class="row pt-3">
	<div class="col-6">
		{{ Form::open(['route' => 'reservation.create']) }}
			<div class="form-group">
				<label for="reservation-name">Naziv rezervacije <small>(Rezervacije brez naziva bodo prikazane z vašim imenom)</small></label>
				{{Form::text('res_name', "", ['class' => 'form-control', 'id' => 'reservation-name', 'placeholder' => 'Naziv rezervacije'])}}
			</div>
			<div class="form-group">
				<label for="tennis-fields">Tenis igrišče</label>
				{{Form::select('field', $fields, null, ['class' => 'form-control', 'id' => 'tennis-fields'])}}
			</div>
			<div class="form-group">
				<label for="date">Datum</label>
				{{Form::text('date', "", ['class' => 'form-control', 'id' => 'datepicker', 'autocomplete' => 'off'])}}
			</div>
			<div class="form-group">
				<label for="time-start">Začetek termina</label>
				{{Form::select('time_start', ['7:00' => '7:00', '7:30' => '7:30', '8:00' => '8:00', '8:30' => '8:30', '9:00' => '9:00', '9:30' => '9:30', '10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30', '12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30', '14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30', '16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30', '18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30', '20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00'], null, ['class' => 'form-control', 'id' => 'time-start'])}}
			</div>
			<div class="form-group">
				<label for="time-end">Konec termina</label>
				{{Form::select('time_end', ['7:00' => '7:00', '7:30' => '7:30', '8:00' => '8:00', '8:30' => '8:30', '9:00' => '9:00', '9:30' => '9:30', '10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30', '12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30', '14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30', '16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30', '18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30', '20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00'], null, ['class' => 'form-control', 'id' => 'time-end'])}}
			</div>
			@if ($phone == false)
			<div class="form-group">
				<label for="Phone">Telefonska številka</label>
				{{Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone', 'placeholder' => 'Telefonska številka'])}}
			</div>
			@else
				{{Form::hidden('phone', $phone)}}
			@endif
			{{Form::submit('Rezerviraj', ['class' => 'btn btn-primary'])}}
		{{Form::close()}}
	</div>
</div>
@stop