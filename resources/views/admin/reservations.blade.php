@extends ('layouts.admin')
@section ('content')
<h2>Čakajoče rezervacije</h2>
@include ('shared.reservations', ['reservations' => $reservations, 'action' => true])

<h2>Opravljene rezervacije</h2>
@include ('shared.reservations', ['reservations' => $confirmed, 'action' => 'delete'])
@stop