<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

	public function user()
	{
		return $this->belongsTo ('App\User')->withDefault();
	}

	public function field()
	{
		return $this->belongsTo ('App\Field')->withDefault();
	}

	public function isReserved ($date, $from, $to)
	{
	}
}