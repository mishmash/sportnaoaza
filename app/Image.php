<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $fillable = ['gallery_id', 'name', 'original_name', 'size', 'mime', 'path'];

	public function gallery ()
	{
		return $this->belongsTo('App\Gallery');
	}
}