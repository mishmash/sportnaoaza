<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated (Request $request, $user)
    {
        if (!$user->verified && $user->role != 'admin') {
            auth()->logout();
            return back()->with(['warning' => 'Potrditi morate vaš račun za prijavo.']);
        }

        if ($user->role == 'admin') {
            return redirect()->intended('/admin/reservations');
        }
        return redirect()->intended('/');
    }

    public function authenticate (Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        }
    }
}
