<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Socialite;

class AuthController extends Controller
{
	public function redirectToProvider ($provider)
	{
		return Socialite::driver($provider)->redirect();
	}

	public function handleProviderCallback ($provider)
	{
		$user = Socialite::driver($provider)->stateless()->user();
		$authUser = $this->findOrCreateUser ($user, $provider);

		Auth::login($authUser, true);
		return redirect('/#');
	}

	public function findOrCreateUser ($user, $provider)
	{
		$authUser = User::where('provider_id', $user->id)->first();
		if ($authUser) {
			return $authUser;
		}

		return User::create([
			'name'        => $user->getName(),
			'email'       => $user->getEmail(),
			'provider'    => $provider,
			'provider_id' => $user->id,
			'role'        => 'user'
		]);
	}
}
