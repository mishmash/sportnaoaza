<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Field;
use App\User;
use App\Gallery;
use App\Image;
use App\Http\Requests\ReserveRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReservationCreated;
use App\Mail\ReservationConfirmed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;
use Uuid;

class AdminController extends Controller
{
	protected $layout = 'layouts.admin';
	
	public function dashboard ()
	{
		return view('admin.dashboard');
	}

	public function reservations ()
	{
		$reservations = Reservation::whereStatus('pending')->orderBy('res_date', 'asc')->get();
		$confirmed    = Reservation::whereStatus('approved')->orderBy('res_date', 'asc')->get();
		return view('admin.reservations', [
			'reservations' => $reservations,
			'confirmed'    => $confirmed
		]);
	}

	public function users ()
	{
		$users   = User::whereRole('user')->get();
		$members = User::whereRole('member')->get();

		return view ('admin.users', [
			'users'   => $users,
			'members' => $members
		]);
	}

	public function createMember ($id)
	{
		$user = User::find($id);
		if (!$user)
			return Redirect::back()->with(['error' => 'Ta uporabnik ne obstaja.']);

		if ($user->role == 'member')
			$user->role = 'user';
		else if ($user->role == 'user')
			$user->role = 'member';
		$user->save();

		return redirect()->intended('/admin/users')
				->with(['status' => 'Status uporabnika je bil spremenjen.']);
	}

	public function addReservation ()
	{
		$fields = Field::pluck('name', 'slug');
		$phone = false;
		if (Auth::check()) {
			$phone = Auth::user()->phone;
			if (empty($phone))
				$phone = false;
		}
		return view ('admin.reservation.add', [
			'fields' => $fields,
			'phone' => $phone
		]);
	}

	public function confirmReservation ($rid)
	{
		$reservation = Reservation::whereRid($rid)->first();
		if (!$reservation) {
			return redirect ('/admin/reservations')->with (
				['error' => 'Rezervacija ne obstaja.']
			);
		}
		
		$reservation->status = 'approved';
		$reservation->save();

		Mail::to($reservation->user->email)->send(new ReservationConfirmed($reservation));

		return redirect('/admin/reservations')->with(['status' => 'Rezervacija je bila potrjena.']);
	}

	public function deleteReservation ($rid)
	{
		$reservation = Reservation::whereRid($rid)->first();
		if (!$reservation) {
			return redirect ('/admin/reservations')->with (
				['error' => 'Rezervacija ne obstaja.']
			);
		}
		$reservation->delete();
		return redirect('/admin/reservations')->with(['status' => 'Rezervacija izbrisana.']);
	}

	public function deleteUser ($id)
	{
		$user = User::find($id);
		if (!$user) {
			return redirect('/admin/users')->with([
				'error' => 'Uporabnik ne obstaja.'
			]);
		}
		$user->delete();
		return redirect('/admin/users')->with(['status' => 'Uporabnik izbrisan.']);
	}

	public function galleries ()
	{
		$galleries = Gallery::all();
		return view ('admin.gallery.index', ['galleries' => $galleries]);
	}

	public function addGallery (Request $request)
	{
		$validated = $request->validate ([
			'gallery_name' => 'required|string|max:64'
		]);

		$gallery = new Gallery;
		$gallery->name = $validated['gallery_name'];
		$gallery->slug = str_slug($gallery->name);

		if ($gallery->save()) {
			return redirect('/admin/galleries')->with(['status' => 'Galerija dodana.']);
		}
	}

	public function viewGallery ($id)
	{
		$gallery = Gallery::where('slug', $id)->first();
		if (!$gallery) {
			return redirect('/admin/galleries')->with(['error' => 'Ta galerija ne obstaja.']);
		}
		return view ('admin.gallery.view')->with('gallery', $gallery);
	}

	public function deleteGallery ($id)
	{
		$gallery = Gallery::where('slug', $id)->first();
		if (!$gallery) {
			return redirect('/admin/galleries')->with(['error' => 'Ta galerija ne obstaja.']);
		}
		$gallery->delete();
		return redirect('/admin/galleries')->with(['status' => 'Galerija je bila izbrisana.']);
	}

	public function uploadImage (Request $request)
	{
		$file = $request->file('file');

		$filename = uniqid() . '.' . $file->getClientOriginalExtension();

		$file->move ('gallery/images', $filename);

		$gallery = Gallery::where('slug', $request->input('gallery_slug'))->first();
		$image = $gallery->images()->create([
			'gallery_id'    => $gallery->id,
			'name'          => $filename,
			'original_name' => $file->getClientOriginalName(),
			'size'          => $file->getClientSize(),
			'mime'          => $file->getClientMimeType(),
			'path'          => 'gallery/images/' . $filename
		]);
		return $image;
	}

	public function deleteImage ($id)
	{
		$image = Image::find($id);
		if (!$image) {
			return Redirect::back()->with(['error' => 'Slika ne obstaja.']);
		}
		$path = public_path() . DIRECTORY_SEPARATOR . $image->path;
		if (File::delete($path)) {
			$image->delete();
			return Redirect::back()->with(['status' => 'Slika je bila izbrisana.']);
		}
		else {
			return Redirect::back()->with(['error' => 'Napaka pri brisanju slike: ' . $path]);
		}
	}
}