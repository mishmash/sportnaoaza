<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Field;
use App\Http\Requests\ReserveRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReservationCreated;
use Auth;
use Uuid;

class ReservationsController extends Controller
{
	protected $layout = 'layouts.fresh';

	public function reservations ($date = null)
	{
		if ($date == null)
			$date = Carbon::today();
		$date = new Carbon($date);
		$date = $date->toDateString();

		$reservations = [];

		$fields = Field::with(['reservations' => function ($query) use($date) {
			$query->whereDate('res_date', $date);
			$query->where ('status', 'approved');
		}])->get();

		$open    = '7:00';
		$close   = '21:00';
		$t_start = strtotime($open);
		$t_end   = strtotime($close);
		$t_now   = $t_start;

		// return $fields->toJson(JSON_PRETTY_PRINT);

		while ($t_now < $t_end) {
			$slot_begin = date ('H:i', $t_now);
			$t_now      = strtotime('+30 minutes', $t_now);
			$slot_end   = date('H:i', $t_now);
			$slot_name  = $slot_begin . ' - ' . $slot_end;

			$field_reservations = [];

			
			foreach ($fields as $field) {
				$field_reservation = null;
				foreach ($field->reservations as $reservation) {

					$r_start = strtotime($reservation->start_time);
					$r_end   = strtotime($reservation->end_time);
					$s_start = strtotime($slot_begin);
					$s_end   = strtotime($slot_end);

					if ($r_start <= $s_start && $r_end >= $s_end) {
						$field_reservation = [
							'name' => $reservation->name
						];
					}
				}
				$field_reservations[] = [
					'slug' => $field->slug,
					'reserved' => $field_reservation
				];
			}
			$reservations[] = [
				'slot' => $slot_name,
				'fields' => $field_reservations
			];
		}

		$all_fields = [];
		foreach (Field::all() as $field) {
			$all_fields[$field->slug] = $field->name;
		}
		$days = []
		;
		// Assume today
		$current_date = date('Y-m-d');
		for ($i = 0; $i < 7; $i++) {
			$day_num = date('N', strtotime($current_date) + ($i * 86400));
			$format  = date('Y-m-d', strtotime($current_date) + ($i * 86400));
			$days[] = [
				'name'   => $this->dayToName(intval($day_num)) . ' ' . $this->dayAppend($i),
				'form'   => $format,
				'select' => ($format == $date) ? true : false
			];
		}

		$reservations = [
			'fields'       => $all_fields,
			'reservations' => $reservations,
			'days'         => $days
		];

		$phone = false;
		if (Auth::check()) {
			$phone = Auth::user()->phone;
			if (empty($phone))
				$phone = false;
		}

		// return json_encode($reservations, JSON_PRETTY_PRINT);
		return view ('reserve', [
			'reservations' => $reservations,
			'today'        => $date,
			'phone'        => $phone
		]);
	}

	public function reserve(ReserveRequest $request)
	{
		$res_name = $request->input('res_name');
		$date  = $request->input ('date');
		$field = $request->input ('field');
		$start = new Carbon($request->input ('time_start'));
		$end   = new Carbon($request->input ('time_end'));

		if (!$res_name) {
			$res_name = Auth::user()->name;
		}

		$validRange = $end->greaterThan($start);
		if (!$validRange) {
			return Redirect::back()->withErrors ('Izberite veljaven termin.');
		}
		$diff  = $end->diffInMinutes($start);

		// Najmanjši termin 1 ura
		if ($diff < 60) {
			return Redirect::back()->withErrors('Najmanjšo trajanje rezervacije je ena ura!');
		}

		if($this->isReserved($field, $date, $request->input('time_start'), $request->input('time_end'))) {
			return Redirect::back()->withErrors ('Ta termin je že zaseden.');
		}

		$field = Field::whereSlug($field)->first();
		if (!$field) {
			// To se ne bi smelo zgoditi
			return Redirect::back()->withErrors ('To igrišče ne obstaja.');
		}

		$field_id = $field->id;
		$reservation = new Reservation;
		$reservation->rid        = Uuid::generate()->string;
		//$reservation->name       = $request->input('name', Auth::user()->name);
		$reservation->name = $res_name;
		$reservation->field_id   = $field_id;
		$reservation->user_id    = Auth::user()->id;
		$reservation->res_date   = $date;
		$reservation->start_time = $start;
		$reservation->end_time   = $end;
		$reservation->status     = 'pending';

		if (Auth::user()->role == 'admin')
			$reservation->status = 'approved';
		
		$user = Auth::user();
		$user->phone = $request->input('phone');
		$user->save();

		if ($reservation->save()) {

			if ($user->role != 'admin') {
				Mail::to (
					[
						'info@sportnaoazatabor.si'
					]
				)->send(new ReservationCreated($reservation));
			}
			return Redirect::back()
				->with('status', 'Rezervacija je bila posredovana skrbniku. Po potrditvi boste obveščeni.');

		}
		else {
			return Redirect::back()->withErrors('Napaka pri izvajanju rezervacije.');
		}
	}

	private function isReserved ($field, $date, $from, $to)
	{
		DB::enableQueryLog();
		$field    = Field::whereSlug($field)->first();
		$field_id = $field->id;

		$reservations = DB::select(
			'SELECT * FROM reservations WHERE field_id = ?
			AND res_date = ? AND ? < end_time AND ? > start_time AND
			status = \'approved\''
			, [$field_id, $date, $from, $to]);

		if (count($reservations) > 0)
			return true;
		return false;
	}

	public function confirm ($rid)
	{
		$reservation = Reservation::whereRid($rid)->first();
		if (!$reservation) {
			return redirect ('/');
		}
		
		$reservation->status = 'approved';
		$reservation->save();

		return redirect('/')->with(['status' => 'Rezervacija je bila potrjena.']);
	}

	private function dayToName ($number)
	{
		$weekdays = [
			1 => "Ponedeljek", 
			2 => "Torek",
			3 => "Sreda",
			4 => "Četrtek",
			5 => "Petek",
			6 => "Sobota",
			7 => "Nedelja"
		];
		return $weekdays[$number];
	}

	private function dayAppend ($number)
	{
		if ($number == 0)
			return '(danes)';
		if ($number == 1)
			return '(jutri)';
		return '';
	}
}