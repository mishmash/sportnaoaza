<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Reservation;
use Carbon\Carbon;

class ReservationConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->reservation->start_time = new Carbon($this->reservation->start_time);
        $this->reservation->end_time   = new Carbon($this->reservation->end_time);

        return $this->view('emails.reservations.confirmed')->with([
                        'field'  => $this->reservation->field->name,
                        'user'   => $this->reservation->user->name,
                        'name'   => $this->reservation->name,
                        'from'   => $this->reservation->start_time->format('H:i'),
                        'to'     => $this->reservation->end_time->format('H:i'),
                        'date'   => $this->reservation->res_date,
                        'rid'    => $this->reservation->rid,
                        'phone'  => $this->reservation->user->phone
        ])->subject('Rezervacija potrjena');
    }
}
