<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Reservation;

class ReservationCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->reservation->id;

        $role = $this->reservation->user->role;
        if ($role == 'user')
            $role = 'uporabnik';
        else if ($role == 'member')
            $role = 'član';

        return $this->view('emails.reservations.created')
                    ->with([
                        'field'  => $this->reservation->field->name,
                        'user'   => $this->reservation->user->name,
                        'name'   => $this->reservation->name,
                        'from'   => $this->reservation->start_time->format('H:i'),
                        'to'     => $this->reservation->end_time->format('H:i'),
                        'date'   => $this->reservation->res_date,
                        'rid'    => $this->reservation->rid,
                        'phone'  => $this->reservation->user->phone,
                        'role'   => $role
                    ])
                    ->subject("Rezervacija #{$id} čaka na potrditev");
    }
}
