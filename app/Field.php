<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{	
	public function reservations ()
	{
		return $this->hasMany('App\Reservation');
	}

	public function reserve ($user, $from, $to)
	{
	}

	public function allSelectable ()
	{
		$fields = Field::all();
		$selectable = [];
		foreach ($fields as $field) {
			$selectable[$field->slug] = $field->name;
		}
		return $selectable;
	}
}